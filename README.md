# JSF

The JSF tutorial is separated from the main branch due to the sheer amount of topic covered

The ordering is based on the pom.xml modules

## Notes

2. template

   Also contains resource loading (normally and JSF-specific tags), and **web.xml** configurations

4. el

   It's actually independent (not bound to Facelets), but it's so much easier to use with Facelets, and some features do not work out of the box with JSP
 
5. url-parameters

   When you want your site to be stateless (no forms, thus all request are idempotent), use URL parameters to pass data around

6. special

   For topics which does not deserve its own module (too small)
   
7. redirect

   Shows the redirect methods. All of them achieves the same goal, and will remove any **@RequestScoped** data
   
8. mithril-omnifaces-ajax

   h:commandScript accepts a callback argument **oncomplete** that runs a function when it completes. If you did not specify that, you can use **function_name({onevent: function(data){if(data.status=='success' redraw())}})**, but as you can see, it's damn long, and you need to wrap it in a no-argument callback (onclick calls the function for you)
   
   So it's up to the developer to see which makes more sense (**oncomplete** makes things much easier)