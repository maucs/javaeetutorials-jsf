package web;

import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import org.omnifaces.util.Faces;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class Complex {
    @Inject
    @Push
    PushContext basic;

    public void sendMessage() {
        String number = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("number");
        if (number == null) {
            number = "no value";
        }
        basic.send(number.toUpperCase());
    }

    public void sendBigMessage(String payload) {
        basic.send(payload.toUpperCase());
    }

    public void sendWithOmnifacesCommandScript() {
        String age = Faces.getRequestParameter("age");
        basic.send("Sally is " + age + " years old");
    }
}
