package services;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import java.util.concurrent.Future;

@Stateless
public class CountingService {
    @Asynchronous
    public Future<String> delayedMessage() throws Exception {
        Thread.sleep(10000);
        return new AsyncResult<>("bugabugabu");
    }
}