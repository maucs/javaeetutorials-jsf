package web;

import org.omnifaces.cdi.Eager;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

@Eager(viewId = "/index.xhtml")
@RequestScoped
public class EagerHelloRequestScoped {
    @PostConstruct
    public void init() {
        System.out.println("Hello World from " + this.getClass().getName());
    }
}