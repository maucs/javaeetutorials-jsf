package web;

import org.omnifaces.cdi.Eager;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

@Eager
@SessionScoped
public class EagerHelloSessionScoped implements Serializable {
    @PostConstruct
    public void init() {
        System.out.println("Hello World from " + this.getClass().getName());
    }
}