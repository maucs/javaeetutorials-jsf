package web;

import services.CountingService;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.concurrent.Future;

@Named
@SessionScoped
public class HeavyCalculation implements Serializable {
    @EJB
    CountingService service;

    Future<String> value;

    public void run() throws Exception {
        if (value == null)
            value = service.delayedMessage();
    }

    public Future<String> getValue() {
        return value;
    }
}
