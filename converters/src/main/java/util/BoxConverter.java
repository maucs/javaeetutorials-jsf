package util;

import entities.Box;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "boxcnvrtr", forClass = Box.class)
public class BoxConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        // from input
        return new Box(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        // to output
        return ((Box)value).getName();
    }
}
