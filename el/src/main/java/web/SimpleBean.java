package web;

import javax.inject.Named;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Named
public class SimpleBean {
    public String name;
    public CardSuites suites;
    public Map<String, String> details;
    public Properties properties;
    public List list;

    public String getName() {
        return name;
    }

    public CardSuites getSuites() {
        return suites;
    }

    public Map<String, String> getDetails() {
        return details;
    }

    public Properties getProperties() {
        return properties;
    }

    public List getList() {
        return list;
    }

    public String methodCallOne(Integer integer) {
        return integer.toString();
    }

    public String methodCallTwo() {
        return "two";
    }
}
