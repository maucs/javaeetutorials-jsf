package services;

import util.PushEvent;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import java.util.function.Supplier;

@Stateless
public class AdminServices {
    @Inject
    BeanManager manager;

    public void push(String user, String message) {
        manager.fireEvent(new PushEvent(user, message));
    }

    @Asynchronous
    public void pushAsync(Supplier callback) throws InterruptedException {
        Thread.sleep(2000);
        callback.get();
    }
}
