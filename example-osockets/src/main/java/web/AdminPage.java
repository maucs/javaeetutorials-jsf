package web;

import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import services.AdminServices;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;

@Named("admin")
@RequestScoped
public class AdminPage {
    @EJB
    AdminServices services;

    @Inject
    @Push
    PushContext main;

    public void push(String user, String msg) {
        services.push(user, msg);
    }

    public void pushAsync() throws InterruptedException {
        services.pushAsync(() -> main.send("howdydoo", Arrays.asList("bob", "Cat")));
    }
}
