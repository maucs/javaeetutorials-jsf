package web;

import org.omnifaces.cdi.push.SocketEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class SocketLogging {
    public void onOpen(@Observes @SocketEvent.Opened SocketEvent event) {
        String channel = event.getChannel();
        String user = event.getUser();
        System.out.println(channel + ": " + user + " has logged in");
    }

    public void onClose(@Observes @SocketEvent.Closed SocketEvent event) {
        String channel = event.getChannel();
        String user = event.getUser();
        System.out.println(channel + ": " + user + " has logged out");
    }

}
