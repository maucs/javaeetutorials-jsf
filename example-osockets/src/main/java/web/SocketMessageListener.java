package web;

import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;
import util.PushEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

@ApplicationScoped
public class SocketMessageListener {
    @Inject
    @Push
    PushContext main;

    public void onReceivePushEvent(@Observes PushEvent event) {
        main.send(event.getMessage(), event.getUser());
    }
}
