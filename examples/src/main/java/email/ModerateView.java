package email;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@RequestScoped
public class ModerateView {
    private String key;
    private boolean valid;

    public void check() {
        valid = !FacesContext.getCurrentInstance().isValidationFailed() && key.length() > 10;
        // valid = org.omnifaces.util.Faces.isValidationFailed() && key.length() > 10;
        // valid = key != null && key.length() > 10;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isValid() {
        return valid;
    }
}
