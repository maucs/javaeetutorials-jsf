package email;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.inject.Named;

@Named
@RequestScoped
public class Simple {
    @ManagedProperty("#{param.key}")
    private String key;
    boolean valid;

    @PostConstruct
    public void init() {
//        valid = key.length() > 10;
        System.out.println(key);
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
