package email;

import org.omnifaces.cdi.Param;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import static org.omnifaces.util.Faces.isValidationFailed;

@Named
@RequestScoped
public class SimpleOmnifaces {
    @Inject @Param(required = true, requiredMessage = "Please add a token")
    String key;
    boolean valid;

    @PostConstruct
    public void init() {
        if (isValidationFailed()) {
            valid = false;
            return;
        }
        valid = key.length() > 10;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
