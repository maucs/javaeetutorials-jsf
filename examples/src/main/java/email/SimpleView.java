package email;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@RequestScoped
public class SimpleView {
    private String key;
    private Boolean valid;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isValid() {
        if (valid != null) return valid;
        // isValidationFailed() returns true if it fails, remember to invert it
        valid = !FacesContext.getCurrentInstance().isValidationFailed() && key.length() > 10;
        return valid;
    }
}