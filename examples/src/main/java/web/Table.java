package web;

import entities.Item;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class Table implements Serializable {
    @Inject NewItem item;

    private List<Item> list;
    private Item edit;
    private String newValue;

    @PostConstruct
    public void init() {
        // actual implementation retrieves from database
        list = new ArrayList<>();
        list.add(new Item(1, "yellow submarine"));
        list.add(new Item(2, "blue sorcery"));
        list.add(new Item(3, "black ball"));
        list.add(new Item(4, "white teeth"));
        list.add(new Item(5, "jackson 10"));
    }

    public void addWithInjectedItem() {
        Item i = item.getItem();
        int size = (list.size() == 0) ? 1 : list.get(list.size() - 1).getId() + 1;
        i.setId(size);
        list.add(i);
        item.setItem(null); // to empty the field
    }

    public void add() {
        int size = (list.size() == 0) ? 1 : list.get(list.size() - 1).getId() + 1;
        list.add(new Item(size, newValue));
        newValue = null;
    }

    public void delete(Item item) {
        list.remove(item);
    }

    public List<Item> getList() {
        return list;
    }

    public void setList(List<Item> list) {
        this.list = list;
    }

    public Item getEdit() {
        return edit;
    }

    public void setEdit(Item edit) {
        this.edit = edit;
    }

    public void editItem() {
        edit = null;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
}
