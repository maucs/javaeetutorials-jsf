package web;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

@Named
@RequestScoped
public class Bean {
    // make sure the directory is set and created before attempting this example
    final String DIR = "";

    String filename;
    boolean error;

    public void download() throws IOException {
        // or just use Omnifaces Faces.sendFile
        FacesContext instance = FacesContext.getCurrentInstance();
        ExternalContext ctx = instance.getExternalContext();

        File file = new File(DIR, filename);
        System.out.println(file.toString());

        if (file.exists()) {
            ctx.responseReset();
            // optional
            ctx.setResponseContentType(ctx.getMimeType(filename));
            // without it, no download file size
            ctx.setResponseContentLength((int) file.length());
            // required
            ctx.setResponseHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

            OutputStream os = ctx.getResponseOutputStream();
            // write the inputStream of the file to this outputStream
            Files.copy(file.toPath(), os);
            // end with this
            instance.responseComplete();
        }
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
