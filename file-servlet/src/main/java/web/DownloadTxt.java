package web;

import org.omnifaces.servlet.FileServlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.File;

// /media/bob will get /bob as getPathInfo()
@WebServlet("/media/*")
public class DownloadTxt extends FileServlet {
    @Override
    protected File getFile(HttpServletRequest req) throws IllegalArgumentException {
        String pathInfo = req.getPathInfo();

        if (pathInfo == null || pathInfo.isEmpty() || "/".equals(pathInfo))
            throw new IllegalArgumentException();

        return new File(this.getClass().getResource("bob.txt").getFile());
    }
}
