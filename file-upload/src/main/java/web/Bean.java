package web;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@Named
@RequestScoped
public class Bean {
    // make sure the directory is set and created before attempting this example
    final String DIR = "";

    Part file;

    public void upload() {
        try (InputStream is = file.getInputStream()) {
            Files.copy(is, new File(DIR, file.getSubmittedFileName()).toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }
}
