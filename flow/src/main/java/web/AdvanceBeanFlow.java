package web;

import javax.faces.flow.Flow;
import javax.faces.flow.builder.FlowBuilder;
import javax.faces.flow.builder.FlowBuilderParameter;
import javax.faces.flow.builder.FlowDefinition;

public class AdvanceBeanFlow {
    // @Produces
    @FlowDefinition
    public Flow defineBeanFlow(@FlowBuilderParameter FlowBuilder builder) {
        builder.id("", "beanFlow");

        builder.returnNode("returnFromBeanFlow") // used in JSF action
                .fromOutcome("/index"); // points to a page

        builder.switchNode("next-move") // used in JSF action
                .defaultOutcome("/index") // default if none of the switch case hits
                .switchCase().condition("#{true}").fromOutcome("next-node")
                .switchCase().condition("#{'a' eq 'b'}").fromOutcome("never-touch-this");

        // if this flow came from another flow, that flow's
        // param1 can be called using #{flowScope.param1FromSomeFlow}
        builder.inboundParameter("param1FromSomeFlow", "#{flowScope.param1}");

        builder.flowCallNode("call-other-flow") // used in JSF action
                .flowReference("", "flow-b") // select which flow
                .outboundParameter("param", "#{bean.value}"); // export param

        return builder.getFlow();
    }
}
