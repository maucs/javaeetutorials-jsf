package web;

import javax.enterprise.inject.Produces;
import javax.faces.flow.Flow;
import javax.faces.flow.builder.FlowBuilder;
import javax.faces.flow.builder.FlowBuilderParameter;
import javax.faces.flow.builder.FlowDefinition;

public class BeanFlowTwo {
    @Produces
    @FlowDefinition
    public Flow defineBeanFlow(@FlowBuilderParameter FlowBuilder builder) {
        builder.id("", "beanFlow");

        builder.returnNode("returnFromBeanFlow")
                .fromOutcome("/index");

        return builder.getFlow();
    }
}
