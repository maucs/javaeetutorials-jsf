package web;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
public class MessagesBean {
    FacesContext ctx = FacesContext.getCurrentInstance();

    public void newMessage() {
        ctx.addMessage("form2:random", new FacesMessage("This is a nice message"));
    }

    public void manyNullMessages() {
        ctx.addMessage(null, new FacesMessage("Hello World"));
        ctx.addMessage(null, new FacesMessage("Say something"));
        ctx.addMessage(null, new FacesMessage("Goodbye"));
    }
}
