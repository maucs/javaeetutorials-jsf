package web;

import entities.Human;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class SelectManyBean {
    Map<String, Integer> listOfNumbers;
    List<Human> listOfHumans = new ArrayList<>();
    List<Integer> chosenNumbers;
    List<Human> chosenHumans = new ArrayList<>();

    @PostConstruct
    public void init() {
        listOfNumbers = new HashMap<>();
        listOfNumbers.put("Small", 10);
        listOfNumbers.put("Medium", 30);
        listOfNumbers.put("Big", 50);
        listOfNumbers.put("Very Big", 100);
        listOfNumbers.put("Huge", 1000);
        listOfNumbers.put("Over 9000", 9001);

        listOfHumans = new ArrayList<>();
        listOfHumans.add(new Human("Johnny"));
        listOfHumans.add(new Human("Leesa"));
        listOfHumans.add(new Human("Mesa"));
        listOfHumans.add(new Human("Apel"));
        listOfHumans.add(new Human("Bob"));
        listOfHumans.add(new Human("Donkey"));
    }

    public Map<String, Integer> getListOfNumbers() {
        return listOfNumbers;
    }

    public void setListOfNumbers(Map<String, Integer> listOfNumbers) {
        this.listOfNumbers = listOfNumbers;
    }

    public List<Integer> getChosenNumbers() {
        return chosenNumbers;
    }

    public void setChosenNumbers(List<Integer> chosenNumbers) {
        this.chosenNumbers = chosenNumbers;
    }

    public List<Human> getListOfHumans() {
        return listOfHumans;
    }

    public void setListOfHumans(List<Human> listOfHumans) {
        this.listOfHumans = listOfHumans;
    }

    public List<Human> getChosenHumans() {
        return chosenHumans;
    }

    public void setChosenHumans(List<Human> chosenHumans) {
        this.chosenHumans = chosenHumans;
    }
}
