package web;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

@Named
@RequestScoped
public class SelectOneBean {
    String selection;
    Integer grade;
    Map<String, Integer> listOfGrades;
    Boolean discount;

    @PostConstruct
    public void init() {
        listOfGrades = new HashMap<>();
        listOfGrades.put("Best", 100);
        listOfGrades.put("So-so", 70);
        listOfGrades.put("Not too bad", 50);
        listOfGrades.put("Bad", 30);
    }

    public Boolean getDiscount() {
        return discount;
    }

    public void setDiscount(Boolean discount) {
        this.discount = discount;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Map<String, Integer> getListOfGrades() {
        return listOfGrades;
    }

    public void setListOfGrades(Map<String, Integer> listOfGrades) {
        this.listOfGrades = listOfGrades;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }
}
