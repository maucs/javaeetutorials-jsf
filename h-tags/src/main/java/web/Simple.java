package web;

import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

@Named
public class Simple {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void valueChange(ValueChangeEvent event) {
        String old = (String) event.getOldValue();
        String newv = (String) event.getNewValue();
        System.out.println(old + " has become " + newv);
    }
}
