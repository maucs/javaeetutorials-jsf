package web;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;

public class SimpleValueChangeListener implements ValueChangeListener {
    @Override
    public void processValueChange(ValueChangeEvent event) throws AbortProcessingException {
        // do stuffs here
    }
}
