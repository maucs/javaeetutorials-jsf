package web;

import entities.Order;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Named("table")
@SessionScoped
public class TableBean implements Serializable {
    private List<Order> orderList;

    @PostConstruct
    public void init() {
        orderList = new ArrayList<>();
        orderList.add(new Order("A01", "Fish", new BigDecimal(10.99), 10));
        orderList.add(new Order("B01", "Dog Food", new BigDecimal(219), 3));
        orderList.add(new Order("B02", "Chicken", new BigDecimal(7.99), 20));
        orderList.add(new Order("A02", "Cake", new BigDecimal(80), 5));
        orderList.add(new Order("A03", "Unicorn", new BigDecimal(999.99), 1));
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public void delete(Order order) {
        orderList.remove(order);
    }
}
