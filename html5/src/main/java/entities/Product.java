package entities;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class Product {
    List<Object> items = new ArrayList<>();

    public List<Object> getItems() {
        return items;
    }

    public Map getProductAttributes() {
        Map<String, String> result = new HashMap<>();
        result.put("data-popup-title", "The product is available for shipping within 24 hours");
        result.put("data-product-id", "1234");
        return result;
    }
}
