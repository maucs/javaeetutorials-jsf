package web;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.Locale;

@Named
@RequestScoped
public class LocaleChanger {
    public void changeLocaleToEn() {
        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("en"));
    }
}
