package web;

import org.omnifaces.util.Ajax;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.Arrays;

@Named
@RequestScoped
public class Logger {
    public void log() {
        System.out.println("Logged");
    }

    public void call() {
        Ajax.update(""); // like f:ajax render
        Ajax.oncomplete("alert('hello world')");
    }

    public void update() {
        Ajax.data("list", Arrays.asList("foo", "bar", "baz"));
    }
}
