package web;

import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

@Named
@RequestScoped
public class Simple {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void actionListener(ActionEvent event) {
        String id = event.getComponent().getId();
    }

    public void valueChangeEvent(ValueChangeEvent event) {
        if (event.getNewValue() != null) {
        }
    }
}
