package web;

import org.omnifaces.util.Ajax;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.Random;

@Named
@RequestScoped
public class Main {
    public void update() {
        Ajax.data("number", new Random().nextInt());
    }

    public void update2() {
        Ajax.data("name", "blah");
    }
}
