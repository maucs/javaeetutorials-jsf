'use strict';

var data = {
    number: 0,
    name: 'click me to change me'
};

var redraw = function () {
    Object.assign(data, OmniFaces.Ajax.data);
    m.redraw()
};

var Hello = {
    view: function view() {
        return m('p', {onclick: update}, data.number);
    }
};

var Blah = {
    view: function view() {
        return m('p', {onclick: update2}, data.name);
    }
};

m.mount(document.getElementById("main"), Hello);
m.mount(document.getElementById("blah"), Blah);