'use strict';

var data = {
    number: 0,
    name: 'click me to change me'
};

// data is updated outside of Mithril
var redraw = function () {
    Object.assign(data, OmniFaces.Ajax.data);
    m.redraw()
};

// used for f:ajax, need the check for status
var f_ajax_redraw = function (data) {
    console.log(data.status);
    if (data.status === 'success')
        redraw()
};

var Hello = {
    view: function view() {
        return m('p', {onclick: update_number}, data.number);
    }
};

var Blah = {
    view: function view() {
        return m('p', {onclick: update_name}, data.name);
    }
};

window.onload = function () {
    m.mount(document.getElementById("main"), Hello);
    m.mount(document.getElementById("blah"), Blah);
};