package web;

import org.omnifaces.util.Faces;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;

@Named
@RequestScoped
public class Bean {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void redirect() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
    }

    public void redirectGoogle() throws IOException {
        Faces.redirect("https://www.google.com");
    }

    /**
     * Goes back to the same page as the request, making a redirect
     * @return
     */
    public String refresh() {
        return Faces.getViewId() + "?faces-redirect=true";
    }
}
