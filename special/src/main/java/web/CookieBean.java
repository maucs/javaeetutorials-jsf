package web;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Named
public class CookieBean {
    @Inject
    @org.omnifaces.cdi.Cookie
    private String bob;

    Map<String, Object> cookieMap = FacesContext.getCurrentInstance().getExternalContext().getRequestCookieMap();

    public void setCookies() {
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.addCookie(new Cookie("foobarbaz", "savethequeenpls"));

        bob = "hohohaha";
    }
}
