package web;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

/**
 * For when you need to return the same page through managed bean
 */
public class CreateSamePageId {
    public String createSameId() {
        UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
        return view.getViewId() + "?faces-redirect=true";
    }
}
