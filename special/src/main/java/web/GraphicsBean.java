package web;

import org.omnifaces.cdi.GraphicImageBean;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Utils;

import java.io.IOException;
import java.io.InputStream;

@GraphicImageBean
public class GraphicsBean {
    public InputStream getApple() {
        return Faces.getResourceAsStream("/resources/img/apple.jpeg");
    }

    public byte[] getFish() throws IOException {
        return Utils.toByteArray(Faces.getResourceAsStream("/resources/img/fish.jpeg"));
    }
}
