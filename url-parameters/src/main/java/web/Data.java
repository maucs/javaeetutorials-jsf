package web;

import javax.inject.Named;

@Named
public class Data {
    public void onLoad() {
        System.out.println("Do something before loading JSF file");
    }

    public String getData() {
        return "a bunch of data";
    }
}
