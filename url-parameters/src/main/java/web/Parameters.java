package web;

import org.omnifaces.cdi.Param;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

@Named
public class Parameters {
    @Inject @NotNull @Param
    private String data;

    /**
     *
     */
    @PostConstruct
    public void init() {
        System.out.println("Loaded: " + data);
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
