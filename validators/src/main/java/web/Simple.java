package web;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@RequestScoped
public class Simple {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void validateNumberRange(FacesContext context,
                                    UIComponent toValidate,
                                    Object value) {
        if (false) {
            // invalidate the field
            ((UIInput) toValidate).setValid(false);
            FacesMessage message = new FacesMessage("No guesses left!");
            // pass the message to this field, which is rendered by h:message(s)
            context.addMessage(toValidate.getClientId(context), message);
            return;
        }
    }
}
